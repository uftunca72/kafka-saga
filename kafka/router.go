package kafka

import (
	"context"
	"sync"

	"github.com/segmentio/kafka-go"
)

type Router struct {
	Route     []Route
	Consumers Consumer
	Context   context.Context
}

type Route struct {
	Reader       *kafka.Reader
	EventHandler Event
}

func CreateRouter(consumer Consumer, context context.Context) *Router {
	return &Router{
		Consumers: consumer,
		Context:   context,
	}
}

func (r *Router) AddRoute(topic string, eventHandler Event) {
	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  []string{r.Consumers.Broker},
		GroupID:  r.Consumers.GroupID,
		Topic:    topic,
		MinBytes: 10e3,
		MaxBytes: 10e6,
	})

	r.Route = append(r.Route, Route{Reader: reader, EventHandler: eventHandler})
}

func (r *Router) Start() {
	group := sync.WaitGroup{}
	group.Add(len(r.Route))

	for _, route := range r.Route {
		go func(route Route) {
			err := r.Consumers.Consume(r.Context, route)
			if err != nil {
				return
			}

			group.Done()
		}(route)
	}

	group.Wait()
}
