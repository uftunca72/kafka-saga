package kafka

import (
	"context"
	"fmt"
	"log"

	"github.com/segmentio/kafka-go"
)

type Event func(message *kafka.Message) error

type Consumer struct {
	GroupID string
	Broker  string
}

func NewConsumer(groupID string, broker string) *Consumer {
	return &Consumer{
		GroupID: groupID,
		Broker:  broker,
	}
}

func (c *Consumer) Consume(ctx context.Context, route Route) error {
	for {
		fmt.Println("test")
		m, err := route.Reader.FetchMessage(ctx)
		if err != nil {
			fmt.Println(err)
			return err
		}

		err = route.EventHandler(&m)
		if err != nil {
			fmt.Println(err)
			return err
		}

		fmt.Printf("message at topic/partition/offset %v/%v/%v: %s = %s\n", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))
		if err := route.Reader.CommitMessages(ctx, m); err != nil {
			log.Fatal("failed to commit messages:", err)
			return err
		}
	}
}
